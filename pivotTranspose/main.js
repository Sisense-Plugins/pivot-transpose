//	Initialize the copyWidget object
prism.run([function() {

	/************************/
	/* Init Variables 		*/
	/************************/

	var supportedChartType = "pivot";
	var propName = "transpose";
	var propLabel = "Transpose";
	var defaultSettings = {
		enabled : false
	}	

	/************************/
	/* Business Logic 		*/
	/************************/

	function getMetadata(widget){

		//	Create an empty object to return
		var results = {
			'rows': {},
			'columns': {},
			'values': {}
		}

		//	Figure out the rows
		var rows = widget.metadata.panel('rows');
		results.rows = {
			count: rows.items.length,
			maxFidx: (rows.items.length>0) ? rows.items[rows.items.length-1].field.index : -1,
			items: rows.items
		}

		//	Figure out the columns
		var columns = widget.metadata.panel('columns');
		results.columns = {
			count: columns.items.length,
			maxFidx: (columns.items.length>0) ? columns.items[columns.items.length-1].field.index : -1,
			items: columns.items
		}

		//	Figure out values
		var values = widget.metadata.panel('values');
		results.values = {
			count: values.items.length,
			items: values.items
		}

		//	Return the results
		return results;
	}

	function transpose(dashboard, args){
		
		//	Should we run the transpose?
		var isSupported = (args.widget.type == supportedChartType);
		var isTranspose = $$get(args.widget.options, propName + '.enabled');
		if (isSupported && isTranspose){

			//	Figure out the metadata for this widget
			var meta = getMetadata(args.widget);

			//	Create an in memory html table
			var table = $('<div></div>').append(args.result.table);

			$('table', table).attr('')

			//	Remove exess column headers (for values)
			var numColumnItems = meta.columns.count;
			$('thead>tr:gt(' + (numColumnItems-1) + ')', table).remove();

			//	Reset the leftover column headers to be skinnier
			$('thead>tr>td', table).each(function(index){

				//	Determine the new column span
				//var newColumnSpan = $(this).attr("colspan") ? parseInt($(this).attr("colspan"))-(meta.values.count-1) : 1;
				//$(this).attr("colspan",newColumnSpan);

				$(this).removeAttr("colspan");

				//	Adjust the row span, if necessary
				if ($(this).attr('rowspan')){
					//$(this).attr('rowspan',1)
					$(this).removeAttr('rowspan');
				}

				//	Is this a dimension column?
				if (parseInt($(this).attr('fidx')) >= 0) {
	
					//	Set fDimension index
					$(this).attr('fDimensionIdx', $(this).attr('fidx'));

					//	Set the index
					$(this).attr('id', 'pivot__C' + (index-1));

					//	Set proper css class
					$(this).attr('class', 'p-dim-member-head p-measure-head');
				}
			})

			//	Get the list of rows,
			var rows = $('tbody>tr', table);
			for (var i=rows.length; i>=0; i--){

				//	Get this row
				var myRow = $('tbody > tr:nth-child(' + i + ')', table);

				//	Loop through all the values
				for (var j=meta.values.count-1; j>=0; j--){

					//	Create a copy
					var newRow = j>0 ? myRow.clone(true) : myRow;

					//	Reference this item
					var newItem = meta.values.items[j];

					//	Adjust the row label
					if (meta.rows.maxFidx >= 0) {
						
						//	There are 1 or more row items
						var cellDiv2 = $('td[fidx=' + meta.rows.maxFidx + ']',newRow);
						cellDiv2.text(cellDiv2.text() + ': ' + newItem.jaql.title);
					} else {

						//	No row items
						var cellDiv2 = $('td:nth-child(1)',newRow);
						cellDiv2.text(newItem.jaql.title);
					}

					//	Remove extra cells from this row
					meta.values.items.forEach(function(item){

						//	Make sure we skip the current value
						if (item.field.index !== newItem.field.index){

							//	Remove unnecessary cells
							$('td[fidx=' + item.field.index + ']',newRow).remove();
						}
					})

					//	Insert the new row
					if (j>0){
						myRow.after(newRow);
					}
				}
			}


			//	Save the table back to the query result
			args.result.table = table.html();
		}
	}

	/************************/
	/* Event Registration 	*/
	/************************/
	
	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		//args.dashboard.on("widgetinitialized", onWidgetInitialized);

		args.dashboard.off("widgetprocessresult", transpose);
		args.dashboard.on("widgetprocessresult", transpose);
		
	});

	/*
	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		//	Hooking to ready/destroyed events
		args.widget.on("destroyed", onWidgetDestroyed);

		//	Should we run a quadrant analysis?
		var shouldInit = (args.widget.type === supportedChartType) && args.widget.options[propName] && args.widget.options[propName].enabled;

		//	Add hook for the quadrant analysis		
		if (shouldInit) {
			args.widget.off("processresult", transpose);
			args.widget.on("processresult", transpose);
		}
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("destroyed", onWidgetDestroyed);
	}
	*/

	//	Create Options for the quadrant analysis
	prism.on("beforemenu", function (e, args) {		
		try {

			//	Has the menu been added already?
			var addMenuItem = true;
			$.each(args.settings.items, function(){
				if (this.caption === propLabel) {
					addMenuItem = false;
				}
			})

			//	Only show this in the widget editor
			var widgetEditorOpen = (prism.$ngscope.appstate == "widget");
			if (!widgetEditorOpen) {
				addMenuItem = false;
				return;
			}
			
			// find the widget
			var widget = e.currentScope.widget;
			if(widget == undefined){
				addMenuItem = false;
				return;
			}	

			// Widget must be a pivot chart
			if(widget.type !== supportedChartType){
				addMenuItem = false;
				return;
			}

			//	Make sure the user clicked on a widget menu
			var target = args.ui.target;
			if (target.attr('title') !== 'Options'){
				addMenuItem = false;
			}

			//	Add the option to enable the quadrant analysis
			if (addMenuItem) {

				//	Get Saved Quadrant Analysis settings
				var settings = widget.options[propName];
				if (typeof settings === 'undefined'){
					//	Create defaults, if not defined
					settings = defaultSettings;
				}
				
				//	Function to handle events
				var menuItemClicked = function(){
					//	Update the settings				
					switch(this.caption){
						case 'Enabled': 
							this.settings.enabled = !this.settings.enabled;
							break;			
					}
					//	Save to widget
					widget.options[propName] = this.settings;
					//	Redraw the widget
					widget.redraw();
				}

				//	Create header
				var header = {
					caption: propLabel,
					type: "header",
					disabled: false
				};

				//	Create Enable option
				var enabled = {
					caption: "Enabled",
					checked: settings.enabled,
					closing: false,
					disabled: false,
					size: "xl",
					type: "check",
					widget : widget,
					settings : settings,
					execute: menuItemClicked
				};
				
				//	Add options to the menu
				args.settings.items.push(header);
				args.settings.items.push(enabled);	
			}

		}
		catch (e) {
			console.log(e);
		}
	});

}]);